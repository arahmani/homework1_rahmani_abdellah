import numpy as np
import scipy 
import matplotlib.pyplot as plt
from optimizer import S
import argparse

parser = argparse.ArgumentParser(prog='PROG')

parser.add_argument('-A', action='store', type=float, nargs='+', help="The matrix A")
parser.add_argument('-nrows', action='store', type=int)
parser.add_argument('-b', action='store', type=float, nargs='+',help="The vector b")
parser.add_argument('-x_0', action='store', type=float, nargs='+',help="The starting point")
parser.add_argument('-maxiter', action='store', type=int, default=1000, help="Outer iteration")
parser.add_argument('-inniter', action='store', type=int, default=2, help="inner iteration")
parser.add_argument('-plot', action='store', type=bool)
args = parser.parse_args()


'''''''''''''''''''''''''''''''''''
'        Arnoldi Function         '
'''''''''''''''''''''''''''''''''''

def arnoldi( A, Q, k ):
    h = np.zeros( k+2 )
    q = np.einsum('ij,j',A,Q[:,k]) 
    for i in range ( k+1 ):
        h[i] = np.einsum('i,i', q, Q[:,i])
        q = q - h[i] * Q[:, i]
    h[ k+1 ] = np.linalg.norm(q)
    q = q / h[ k+1 ]
    return h, q 

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'           Applying Givens Rotation to H col           '
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''

def apply_givens_rotation( h, cs, sn, k ):
    for i in range( k-1 ):
        temp   =  cs[i] * h[i] + sn[i] * h[i+1]
        h[i+1] = -sn[i] * h[i] + cs[i] * h[i+1]
        h[i]   = temp

    # update the next sin cos values for rotation
    cs_k, sn_k = givens_rotation_1( h[k-1], h[k] )
    
    # eliminate H[ k+1, i ]
    h[k] = cs_k * h[k] + sn_k * h[k + 1]
    h[k + 1] = 0.0

    return h, cs_k, sn_k

##----Calculate the Given rotation matrix----##
def givens_rotation_1(v1,v2):
    if v1 ==0:
        cs, sn = 0,1
        return cs,sn
    else:
        t = np.sqrt((v1**2) + (v2**2))
        cs = v1 / t
        sn = v2 / t
        return cs,sn
    
def GMRES(A, b, x_0, max_outer_iter=2, max_iterations=1000, threshold = 1e-5, return_step = False):
    """Function that find a solution to the equation Ax = b

    Args:
        A (_ndarray_): _our matrix_
        b (_ndarray_): _vector of the equation or the optimization problem_
        x_0 (_ndarray_): _starting point_
        max_outer_iter (int, optional): number of outer iteration. Defaults to 2.
        max_iterations (int, optional): number of minimum iteration. Defaults to 1000.
        threshold (_type_, optional): Chosen threshold. Defaults to 1e-5.
        return_step (bool, optional): an option in order to return the optimization step. Defaults to False.

    Returns:
        _ndarray_: optimal solution of Ax = b
    """
    steps = [x_0]
    S_step_sol = [S(x_0, A, b)]
    for _ in range(max_outer_iter):
        x = x_0
        n = len( A )
        m = max_iterations

        r = b - np.einsum('ij,j', A,x) 

        r_norm = np.linalg.norm( r )

        b_norm = np.linalg.norm( b )

        error = np.linalg.norm( r ) / b_norm
        e = [error]
        
        # initialize the 1D vectors 
        sn = np.zeros( m )
        cs = np.zeros( m )
        e1 = np.zeros( m + 1 )
        e1[0] = 1.0

        beta = r_norm * e1 
        # beta is the beta vector instead of the beta scalar

        H = np.zeros(( m+1, m+1 ))
        Q = np.zeros((   n, m+1 ))
        Q[:,0] = r / r_norm

        for k in range(m):

            ( H[0:k+2, k], Q[:, k+1] )    = arnoldi(A, Q, k)
            ( H[0:k+2, k], cs[k], sn[k] ) = apply_givens_rotation( H[0:k+2, k], cs, sn, k)
            
            # update the residual vector
            beta[ k+1 ] = -sn[k] * beta[k]
            beta[ k   ] =  cs[k] * beta[k]

            # calculate and save the errors
            error = abs(beta[k+1]) / b_norm
            e = np.append(e, error)

            if( error <= threshold):
                break
        # calculate the result
        y = np.einsum('ij,j',np.linalg.inv( H[0:k+1, 0:k+1]), beta[0:k+1])
        x = x + np.einsum('ij,j',Q[:,0:k+1],y)
        x_0 = x 
        steps.append(x)
        S_step_sol.append(S(x, A, b))
    if return_step == True:
        return x, np.stack(steps), np.array(S_step_sol)
    else:
        return x
def plot_function(A, b, x_0):
    """Function that plot the function 1/2 x.T(Ax) - bx as well as the optimization step that GMRES took to solve the equation

    Args:
        A (_ndarray_): _our matrix_
        b (_ndarray_): _vector of the equation or the optimization problem_
        x_0 (_ndarray_): _starting point_
    """
    x = np.linspace(-3, 3, 100)
    y = np.linspace(-3, 3, 100)
    X, Y = np.meshgrid(x, y)
    dim_0, dim_1 = X.shape
    Data_points = np.stack([X.reshape((dim_0, dim_1,1)),Y.reshape((dim_0, dim_1,1))], axis=2).reshape((dim_0,dim_1, 2))
    S_solu = np.array([[S(j, A, b) for j in x] for x in Data_points])

    _ , steps, sol = GMRES(A,b,x_0, 1000, 2, return_step = True)
    array_steps = np.zeros((sol.shape[0],3))
    array_steps[:,:2] = steps
    array_steps[:,2] = sol
        
    fig = plt.figure(figsize = (15,8))
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(array_steps[:,0], array_steps[:,1], array_steps[:,2], "--o",color = "red")
    ax.plot_surface(X, Y, S_solu, rstride=1, cstride=1,alpha = .4,
    cmap='binary', edgecolor='none')
    ax.set_title("Function S", fontsize = 13)
    ax.set_xlabel('x', fontsize = 11)
    ax.set_ylabel('y', fontsize = 11)
    ax.set_zlabel('S', fontsize = 11)
    ax.view_init(40, 70)
    plt.draw()
    plt.show()
    
if __name__=='__main__':
    
    resu = GMRES(np.array(args.A).reshape((args.nrows,args.nrows)) 
                             , np.array(args.b), np.array(args.x_0), args.maxiter, args.inniter)
    print("result " + str(resu))
    if args.plot == True:
        plot_function(np.array(args.A).reshape((args.nrows,args.nrows)), 
                      np.array(args.b),  np.array(args.x_0))