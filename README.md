# Homework 1 by: Rahmani Abdellah

In this repository, you will find two python script, the first one named ```optimizer.py``` in which we implement the first exercise and the second one ```GRMES``` in which we implement the second exercice.

## How to use it

you can clone the repository using the command as follows:
```
git clone https://gitlab.epfl.ch/arahmani/homework1_rahmani_abdellah.git
```

## Run the scripts

In order to run the scripts you can use the following command :
* For the first file ```optimizer.py```:
```python optimizer.py -A 8 1 1 3 -nrows 2 -b 2 4 -x_0 0 0 -method "BFGS" -optim "lgmres" -plot True````
Run ``python optimizer.py -h```to understand the different variables 
* For the first file ```optimizer.py```:
```python GRMES.py -A 8 1 1 3 -nrows 2 -b 2 4 -x_0 0 0 -plot True```

## Functions
All the function are documented you can use help(file.function) to learn more about the functions


## Dependencies

You will find a ```requirements.txt```file. in order to install all the requirements use the following command:
```pip install -r requirements.txt```