import opcode
import numpy as np
import scipy 
import matplotlib.pyplot as plt

import argparse

parser = argparse.ArgumentParser(prog='PROG')

parser.add_argument('-A', action='store', type=float, nargs='+', help="The matrix A")
parser.add_argument('-nrows', action='store', type=int)
parser.add_argument('-b', action='store', type=float, nargs='+',help="The vector b")
parser.add_argument('-x_0', action='store', type=float, nargs='+',help="The starting point")
parser.add_argument('-method', action='store', type=str, nargs='+',help="solver for optimize")
parser.add_argument('-optim', action='store', type=str, nargs='+',help="optimizer choice")
parser.add_argument('-plot', action='store', type=bool, help="if you want to plot the steps")
args = parser.parse_args()
A =  np.array(args.A).reshape((args.nrows,args.nrows))  

b =  np.array(args.b) 

def S(x, A, b):
    S = 0.5*(x.T.dot(A.dot(x))) -x.T.dot(b)
    return S

list_optimize = []
list_lgmres = []

def callback_optimize(x_t):
    Nfeval = 1
    list_optimize.append([x_t[0],x_t[1], S(x_t, A, b)])
    Nfeval += 1
    
def callback_lgmres(x_t):
    Nfeval = 1
    list_lgmres.append([x_t[0],x_t[1], S(x_t, A, b)])
    Nfeval += 1
    
def optimize_function(S, A, b, method, x_0, optim):
    """optimization function that solve the target problem

    Args:
        S (_function_): _The function that we want to optimize_
        A (_ndarray_): _our matrix_
        b (_ndarray_): _vector of the equation or the optimization problem_
        method (_str_): _optimize solver_
        x_0 (_ndarray_): _starting point_
        optim (_str_): _optimizher choice between lgmres or optimize_

    Returns:
        _ndarray_: _optimal solution_
    """
    global list_optimize
    list_optimize = []
    global list_lgmres
    list_lgmres = []
    if optim == "optimize":
        result = scipy.optimize.minimize(S,  x_0, args = (A,b), method = method, callback=callback_optimize)
        print("Success state: " +str(result.success))
        return result
    elif optim == "lgmres":
        result, info = scipy.sparse.linalg.lgmres(A, b, x_0, atol=1e-05, callback=callback_lgmres)
        if info > 0:
            print("convergence to tolerance not achieved, number of iterations")
        elif info < 0:
            print("illegal input or breakdown")
        else:
            print("successful exit")
        return result
    else:
        print("The chosen method isn't implemented in this script")
        return None
    
def plot_function(A, b, method, x_0, chose_optimizer):
    """Plotting function that plot the function as well as the optimization steps 

    Args:
        A (_ndarray_): _our matrix_
        b (_ndarray_): _vector of the equation or the optimization problem_
        method (_str_): _optimize solver_
        x_0 (_ndarray_): _starting point_
        chose_optimizer (_str_): _optimizher choice between lgmres or optimize_
    """
    x = np.linspace(-3, 3, 100)
    y = np.linspace(-3, 3, 100)
    X, Y = np.meshgrid(x, y)
    dim_0, dim_1 = X.shape
    Data_points = np.stack([X.reshape((dim_0, dim_1,1)),Y.reshape((dim_0, dim_1,1))], axis=2).reshape((dim_0,dim_1, 2))
    S_solu = np.array([[S(j, A, b) for j in x] for x in Data_points])
    if chose_optimizer == "optimize":
        res = optimize_function(S, A, b,method, x_0, "optimize")
        array_steps = np.array(list_optimize)
    elif chose_optimizer == "lgmres":
        res = optimize_function(S, A, b,"BFGS", x_0, "lgmres")
        array_steps = np.array(list_lgmres)
        
    fig = plt.figure(figsize = (15,8))
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(array_steps[:,0], array_steps[:,1], array_steps[:,2], "--o",color = "red")
    ax.plot_surface(X, Y, S_solu, rstride=1, cstride=1,alpha = .4,
    cmap='binary', edgecolor='none')
    ax.set_title("Function S", fontsize = 13)
    ax.set_xlabel('x', fontsize = 11)
    ax.set_ylabel('y', fontsize = 11)
    ax.set_zlabel('S', fontsize = 11)
    ax.view_init(40, 70)
    plt.draw()
    plt.show()


if __name__=='__main__':
    resu = optimize_function(S, np.array(args.A).reshape((args.nrows,args.nrows)) 
                             , np.array(args.b), args.method[0], args.x_0, args.optim[0])
    print("result " + str(resu))
    if args.plot == True:
        plot_function(A, b, args.method[0], args.x_0, args.optim[0])